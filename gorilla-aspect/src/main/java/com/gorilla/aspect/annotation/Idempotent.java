package com.gorilla.aspect.annotation;

import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;

/**
 * @author zhanghongzeng
 * 幂等注解，同时请求防并发，多次请求返回相同数据
 * https://www.processon.com/view/link/61848981f346fb2ecc44ee61
 */
@Inherited
@Target(ElementType.METHOD)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface Idempotent {

	String value() default "";

	/**
	 * 幂等操作的唯一标识，使用spring el表达式 用#来引用方法参数
	 * @return Spring-EL expression
	 */
	String key();

	/**
	 * 有效期 默认：5 有效期要大于程序执行时间，否则请求还是可能会进来
	 * @return expireTime
	 */
	int expireTime() default 5;

	/**
	 * 时间单位 默认：天
	 * @return TimeUnit
	 */
	TimeUnit timeUnit() default TimeUnit.DAYS;

	/**
	 * 提示信息，可自定义
	 * @return String
	 */
	String info() default "重复请求，请稍后重试";

	/***
	 * 执行后判断，不缓存的条件。unless 接收一个结果为 true 或 false 的表达式，表达式支持 SpEL。当结果为 true 时，不缓存。
	 * @return
	 */
	String unless() default "#result == null";

}
