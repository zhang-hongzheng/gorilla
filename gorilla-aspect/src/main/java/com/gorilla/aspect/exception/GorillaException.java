/*
 *
 *      Copyright (c) 2018-2025, eshop All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the eshop.com developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: eshop
 *
 */

package com.gorilla.aspect.exception;

import lombok.NoArgsConstructor;

/**
 * @author eshop
 * @date 😴2018年06月22日16:21:57
 */
@NoArgsConstructor
public class GorillaException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public GorillaException(String message) {
		super(message);
	}


	public GorillaException(Throwable cause) {
		super(cause);
	}

	public GorillaException(String message, Throwable cause) {
		super(message, cause);
	}

	public GorillaException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	@Override
	public synchronized Throwable fillInStackTrace() {
		return this;
	}

}
