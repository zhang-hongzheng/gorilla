package com.gorilla.aspect.util;

import lombok.experimental.UtilityClass;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.LocalVariableTableParameterNameDiscoverer;
import org.springframework.expression.Expression;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import java.lang.reflect.Method;

@UtilityClass
public class KeyResolverUtil {


    private static final SpelExpressionParser PARSER = new SpelExpressionParser();

    private static final LocalVariableTableParameterNameDiscoverer DISCOVERER = new LocalVariableTableParameterNameDiscoverer();



    public String resolver(String expressionStr, JoinPoint point){
        Object[] arguments = point.getArgs();
        String[] params = DISCOVERER.getParameterNames(getMethod(point));
        StandardEvaluationContext context = new StandardEvaluationContext();

        for (int len = 0; len < params.length; len++) {
            context.setVariable(params[len], arguments[len]);
        }

        Expression expression = PARSER.parseExpression(expressionStr);
        return expression.getValue(context, String.class);
    }

    public Boolean resolver(String unless, Object result){
        StandardEvaluationContext context = new StandardEvaluationContext();
        context.setVariable("result", result);

        Expression expression = PARSER.parseExpression(unless);
        return expression.getValue(context, Boolean.class);
    }

    /**
     * 根据切点解析方法信息
     * @param joinPoint 切点信息
     * @return Method 原信息
     */
    private Method getMethod(JoinPoint joinPoint) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        if (method.getDeclaringClass().isInterface()) {
            try {
                method = joinPoint.getTarget().getClass().getDeclaredMethod(joinPoint.getSignature().getName(),
                        method.getParameterTypes());
            }
            catch (SecurityException | NoSuchMethodException e) {
                throw new RuntimeException(e);
            }
        }
        return method;
    }
}
